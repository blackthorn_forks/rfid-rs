//! Blocking implementations for the three supported communication interfaces.

pub mod i2c;
pub mod spi;
pub mod uart;
